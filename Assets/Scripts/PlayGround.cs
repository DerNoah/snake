﻿using System;
using System.Collections.Generic;

public class PlayGround
{
    private static Field[,] fields;
    public static List<InsurmountableObstacle> obstaclesOnField = new List<InsurmountableObstacle>();
    public static Field[,] playableFields;
    
    public static Field RandomEmptyField(out Tuple<int, int> calculatedIndex)
    {
        if (playableFields.Length > 0)
        {
            int randomIndex1 = (int)UnityEngine.Random.Range(0, playableFields.GetUpperBound(0));
            int randomIndex2 = (int)UnityEngine.Random.Range(0, playableFields.GetUpperBound(1));

            UnityEngine.Debug.Log(playableFields[randomIndex1, randomIndex2]?.GameObject);

            while (playableFields[randomIndex1, randomIndex2] != null || playableFields[randomIndex1, randomIndex2]?.GameObject != null)
            {
                randomIndex1 = (int)UnityEngine.Random.Range(0, playableFields.GetUpperBound(0));
                randomIndex2 = (int)UnityEngine.Random.Range(0, playableFields.GetUpperBound(1));
            }

            UnityEngine.Debug.Log("x:" + randomIndex1 + " ; y:" + randomIndex2);
            calculatedIndex = new Tuple<int, int>(randomIndex1, randomIndex2);
            return playableFields[calculatedIndex.Item1, calculatedIndex.Item2];
        }
        else
        {
            calculatedIndex = null;
            return null;
        }
    }

    public PlayGround(int sizeX, int sizeY)
    {
        fields = new Field[sizeX, sizeY];
        playableFields = new Field[sizeX - 2, sizeY - 2];

        //Creates the Border of the PlayGround
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                fields[x, y] = new InsurmountableObstacle(new Tuple<int, int>(x, y));
                fields[x, y].GameObject.name = "BorderObject";
            }
        }
        for (int i = 1; i < fields.GetUpperBound(0); i++)
        {
            for (int j = 1; j < fields.GetUpperBound(1); j++)
            {
                InsurmountableObstacle obstacle = fields[i, j] as InsurmountableObstacle;

                obstacle.Destroy();
                fields[i, j] = null;
                playableFields[i - 1, j - 1] = fields[i, j];
            }
        }
    }
}














//private static InsurmountableObstacle[] m_borderObjects;
//public static InsurmountableObstacle[] BorderObjects
//{
//    get { return m_borderObjects; }
//}

////It´s hardcoded
//public static void CreateWithCamView(Camera camera)
//{
//    float actualCamView = camera.orthographicSize;
//    //count of cubes/InsuremountableObestacles
//    //I don´t know why it is -5 but it is for the exact scale
//    int lengthX = (int)actualCamView * 4 -5;
//    int lengthY = (int)actualCamView * 2;
//    //both length values tested with actualCamView = 12

//    //position of horizontal line on y-axis
//    int upperMiddPosY = (int)(lengthY / 2); //+0.1f for a little distance to Snake
//    int lowerMiddPosY = (int)upperMiddPosY * (-1);

//    //position of vertical line on x-axis
//    int rightMiddPosX = (int)(lengthX / 2);
//    int leftMiddPosX = (int)rightMiddPosX * (-1);

//    m_borderObjects = new InsurmountableObstacle[(int)(lengthX * 2 + lengthY * 2)];

//    //build upper border
//    for (int i = 0; i < lengthX; i++)
//    {
//        Tuple<int, int> cubePosition = new Tuple<int, int>(-lengthX / 2 + i, upperMiddPosY);

//        m_borderObjects[i] = new InsurmountableObstacle(cubePosition);
//    }
//    //build lower border
//    for (int i = 0; i < lengthX; i++)
//    {
//        Tuple<int, int> cubePosition = new Tuple<int, int>(-lengthX / 2 + i, lowerMiddPosY);
//        //i+lengthX To not override the previous cubes
//        m_borderObjects[i+lengthX] = new InsurmountableObstacle(cubePosition);
//    }
//}
