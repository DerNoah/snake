﻿using UnityEngine;
using System;
using TupleExtension;

public class InsurmountableObstacle : Field
{
    //Constructor
    public InsurmountableObstacle(Tuple<int, int> pos)
    {
        Position = pos;
        GameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
        GameObject.transform.position = pos.ToVector3();
        GameObject.transform.localScale = Vector3.one;

        PlayGround.obstaclesOnField.Add(this);
    }
    public InsurmountableObstacle(Tuple<int, int> pos, Vector3 scale)
    {
        Position = pos;
        GameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
        GameObject.transform.position = pos.ToVector3();
        GameObject.transform.localScale = scale;

        PlayGround.obstaclesOnField.Add(this);
    }
}