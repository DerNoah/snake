﻿using UnityEngine;
using System;

namespace TupleExtension
{
    public static class TupleMethods
    {
        public static Tuple<int, int> Substract(this Tuple<int, int> tuple1, Tuple<int, int> tuple2)
        {
            if (tuple2 is null)
            {
                tuple2 = new Tuple<int, int>(0, 0);
            }
            return new Tuple<int, int>(tuple1.Item1 - tuple2.Item1, tuple1.Item2 - tuple2.Item2);
        }
        public static Tuple<int, int> Add(this Tuple<int, int> tuple1, Tuple<int, int> tuple2)
        {
            if (tuple2 is null)
            {
                tuple2 = new Tuple<int, int>(0, 0);
            }
            return new Tuple<int, int>(tuple1.Item1 + tuple2.Item1, tuple1.Item2 + tuple2.Item2);
        }
        public static Tuple<int, int> Multiplicate(this Tuple<int, int> tuple1, int multiplier)
        {
            if (tuple1 is null)
            {
                tuple1 = new Tuple<int, int>(0, 0);
            }
            return new Tuple<int, int>(tuple1.Item1 * multiplier, tuple1.Item2 * multiplier);
        }
        /// <summary>
        /// To compare the values of a Tuple and not the storage
        /// </summary>
        /// <param name="tuple"></param>
        /// <param name="comparisonValue"></param>
        /// <returns></returns>
        public static bool EqualsTo(this Tuple<int, int> tuple, Tuple<int, int> comparisonValue)
        {
            bool equalizeN1 = tuple?.Item1 == comparisonValue?.Item1;
            bool equalizeN2 = tuple?.Item2 == comparisonValue?.Item2;

            return equalizeN1 && equalizeN2;
        }

        #region ToVector3 Methods
        public static Vector3 ToVector3(this Tuple<int, int> tuple)
        {
            return new Vector3(tuple.Item1, tuple.Item2);
        }
        public static Vector3 ToVector3(this Tuple<int, int, int> tuple)
        {
            return new Vector3(tuple.Item1, tuple.Item2, tuple.Item3);
        }
        #endregion
    }


    public static class RandomTuple
    {
        public static Tuple<int, int> TupleInt(Tuple<int, int> startRange, Tuple<int, int> endRange)
        {
            if (startRange is null)
            {
                startRange = TupleDirection.Zero;
            }
            if (endRange is null)
            {
                endRange = TupleDirection.Zero;
            }
            return new Tuple<int, int>((int)UnityEngine.Random.Range(startRange.Item1, endRange.Item1), (int)UnityEngine.Random.Range(startRange.Item2, endRange.Item2));
        }
    }
}