﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TupleExtension;


public class Fruit : Field, IIsEatable
{
    public Fruit()
    {
        Field newPlace = PlayGround.RandomEmptyField(out Tuple<int, int>newPos);
        this.GameObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        this.Position = newPos;
        newPlace = this;
    }
}
