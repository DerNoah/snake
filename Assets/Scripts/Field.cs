﻿using TupleExtension;
using UnityEngine;
using System;

public class Field
{
    public GameObject GameObject { get; set; }
    private Tuple<int, int> m_position;

    public Tuple<int, int> Position
    {
        get { return this.m_position; }
        set
        {
            this.m_position = value;

            if (GameObject != null)
            {
                GameObject.transform.position = value.ToVector3();
            }
        }
    }


    public void Destroy()
    {
        this.m_position = null;
        if (this.GameObject != null)
        {
            GameObject.Destroy(this.GameObject);
        }
    }
}
