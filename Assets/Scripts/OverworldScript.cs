﻿using System.Collections;
using UnityEngine;
using System;
using TupleExtension;

public class OverworldScript : MonoBehaviour
{
    public static PlayGround PlayGround;
    public static Snake Snake;

    public float TickSpeed = 0.3f;
    public int FieldSizeX = 12;
    public int FieldSizeY = 12;
    public int StartLength = 4;
    private Coroutine m_coroutine;


    void Start()
    {
        PlayGround = new PlayGround(FieldSizeX, FieldSizeY);
        SetupCamera();
        Snake = new Snake(StartLength);

        m_coroutine = StartCoroutine(MainFunction(TickSpeed));
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && m_coroutine is null)
        {
            m_coroutine = StartCoroutine(MainFunction(TickSpeed));
        }

        //set new Direction
        if (KeyInput(out Tuple<int, int> newDir))
        {
            Snake.Direction = newDir;
        }
    }

    void SetupCamera()
    {
        Camera.main.orthographicSize = FieldSizeY / 2;
        Camera.main.transform.position = new Vector3(FieldSizeX / 2 - 0.5f, FieldSizeY / 2 - 0.5f, -10);
    }


    private bool KeyInput(out Tuple<int, int> direction)
    {
        //Change direction with key input
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            direction = TupleDirection.Left;
            return true;
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            direction = TupleDirection.Right;
            return true;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            direction = TupleDirection.Up;
            return true;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            direction = TupleDirection.Down;
            return true;
        }
        else
        {
            direction = new Tuple<int, int>(0, 0);
            return false;
        }
    }


    private static IEnumerator MainFunction(float tickSpeed)
    {
        int loopCount = new int();
        Fruit fruit = new Fruit();

        while (!Snake.IsStuck())
        {
            if (Snake.Head.Position.EqualsTo(fruit.Position))
            {
                fruit.Destroy();
                Snake.AddTile();
                fruit = new Fruit();
            }

            Snake.MoveForward();

            loopCount++;
            yield return new WaitForSeconds(tickSpeed);
        }
    }
}
