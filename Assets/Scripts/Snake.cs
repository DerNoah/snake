﻿using System.Collections.Generic;
using System;
using UnityEngine;
using TupleExtension;

public class Snake
{
    public List<InsurmountableObstacle> Snakebody = new List<InsurmountableObstacle>();
    public InsurmountableObstacle Head => Snakebody[0];

    private Tuple<int, int> m_Direction;
    public Tuple<int, int> Direction
    {
        get { return m_Direction; }
        set
        {
            if (!value.EqualsTo(m_Direction.Multiplicate(-1)) && value != null && !value.EqualsTo(TupleDirection.Zero))
            {
                if (!Head.Position.Add(value).EqualsTo(Snakebody[1].Position))
                {
                    m_Direction = value;
                }
            }
        }
    }

    public bool IsStuck()
    {
        if (Snakebody.Count <= 1)
        {
            return false;
        }

        foreach (InsurmountableObstacle item in PlayGround.obstaclesOnField)
        {
            //skip first item
            if (item == Snakebody[0])
            {
                continue;
            }
            if (Snakebody[0].Position.Add(this.Direction).EqualsTo(item.Position))
            {
                return true;
            }
        }
        return false;
    }

    public void AddTile()
    {
        Tuple<int, int> lastTilePos = Snakebody[Snakebody.Count - 1].Position;

        // calculate position behind
        Tuple<int, int> newTilePos = lastTilePos.Substract(Direction);

        InsurmountableObstacle newTile = new InsurmountableObstacle(newTilePos, new Vector3(0.95f, 0.95f, 1));//To see each individual cube of the Snake
        newTile.GameObject.name = Snakebody.Count.ToString();
        Snakebody.Add(newTile);
    }

    public void MoveForward()
    {
        Tuple<int, int>[] oldPositions = new Tuple<int, int>[Snakebody.Count];

        for (int i = 0; i < oldPositions.Length; i++)
        {
            oldPositions[i] = Snakebody[i].Position;
        }
                
        Snakebody[0].Position = Snakebody[0].Position.Add(Direction);

        for (int i = 1; i < Snakebody.Count; i++)
        {
            Snakebody[i].Position = oldPositions[i - 1];
        }
    }


    public void EatFruit(Fruit fruit)
    {
        fruit.Destroy();
        this.AddTile();
    }


    //Constructor
    public Snake()
    {
        Snakebody.Add(new InsurmountableObstacle(new Tuple<int, int>(4, 3), new Vector3(0.95f, 0.95f, 1)));//To see each individual cube of the Snake
        this.m_Direction = TupleDirection.Right;
    }
    public Snake(int length)
    {
        Snakebody.Add(new InsurmountableObstacle(new Tuple<int, int>(4, 3), new Vector3(0.95f, 0.95f, 1)));//To see each individual cube of the Snake
        this.m_Direction = TupleDirection.Right;

        for (int i = 0; i < length - 1; i++)
        {
            this.AddTile();
        }
    }
}