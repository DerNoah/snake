﻿using System;

public static class TupleDirection
{
    public static Tuple<int, int> Up => new Tuple<int, int>(0, 1);
    public static Tuple<int, int> Down => new Tuple<int, int>(0, -1);
    public static Tuple<int, int> Left => new Tuple<int, int>(-1, 0);
    public static Tuple<int, int> Right => new Tuple<int, int>(1, 0);
    public static Tuple<int, int> Zero => new Tuple<int, int>(0, 0);
}